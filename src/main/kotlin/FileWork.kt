import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import java.io.File
import java.io.PrintWriter

class FileWork {
    fun writeFile(json: String) {
        var writter = PrintWriter(File("/Users/Saurik/IdeaProjects/lab13Test/src/main/resources/base.bd"))
        writter.print(json)
        writter.close()
    }

    fun readFile(): List<Contact> {
        var reader = File("/Users/Saurik/IdeaProjects/lab13Test/src/main/resources/base.bd").bufferedReader().readLine()
        return Gson().fromJson<List<Contact>>(reader)
    }
}