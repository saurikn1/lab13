import com.google.gson.Gson
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet(name = "Hello", value = "/")
class HomeController : HttpServlet() {
    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {
        val gson = Gson()
        fun write(obj: Any) {
            res.writer.print(obj)
        }
        write(Wrapper().title("Phonebook"))
        write(Wrapper().createTable(FileWork().readFile()))
        write(Wrapper().createAddForm())
        if (req.getParameter("name") != null) {
            if ((req.getParameter("name") != "")&&(req.getParameter("phone")!= ""))
            FileWork().writeFile(gson.toJson(UserList().
                    addContact(Contact(req.getParameter("name"), req.getParameter("phone")), FileWork().readFile())))
            res.sendRedirect("/")
        }

        if (req.getParameter("delIndex") != null) {
                FileWork().writeFile(gson.toJson(UserList().minusContact(FileWork().readFile(), req.getParameter("delIndex").toInt())))
            res.sendRedirect("/")
        }
    }
}