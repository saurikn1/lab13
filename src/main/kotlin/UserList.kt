class UserList {
    fun addContact(contact: Contact, contactList: List<Contact>): List<Contact> {
        return contactList.plusElement(contact)
    }

    fun minusContact(contactList: List<Contact>, index: Int): List<Contact> {
        return contactList.minusElement(contactList.get(index))
    }
}