class Wrapper() {
    fun title(title: String): String {
        return "<h1>$title</h1>"
    }

    fun createTable(users: List<Contact>): String {
        var returnString = "<table border=\"1\" width=\"100%\" cellpadding=\"5\">\n" +
                "   <tr>\n" +
                "    <th>Name</th>\n" +
                "    <th>Phone</th>\n"+
                "    <th>Delete</th>\n"
        for (i in users) {
            returnString += "<tr><td>${i.name}</td><td>${i.number}</td><td><form action=\"/\" method=\"get\">" +
                    "<input type=\"submit\" name = \"delIndex\" value=\"${users.indexOf(i)}\"/>\n</form>" +
                    "</td></tr>"
        }
        returnString += "</table>"
        return returnString
    }

    fun createAddForm(): String {
        return "<form action=\"/\" method=\"get\">\n" +
                "Enter your Name: <input type=\"text\" name=\"name\">\n" +
                "Enter your Phone: <input type=\"text\" name=\"phone\">\n" +
                "<input type=\"submit\" value=\"Submit\" />\n" +
                "</form>"
    }
}